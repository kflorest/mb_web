//PARALAX
var planeLeft = document.getElementById("plane_left");
var planeRight = document.getElementById("plane_right");

function parallax() {
    // planeLeft.style.top = -(window.pageYOffset / 8)+'px';
    // planeRight.style.top = -(window.pageYOffset / 12)+'px';
}
window.addEventListener("scroll",parallax,false);

$(document).ready(function(){
    var $layer_container = $('.mb-layers'),
        $_home = $('#_home'),
        $_menu = $('#_menu'),
        $_nav = $('#_nav'),
        $layer_1 = $('#layer_1'),
        $layer_2 = $('#layer_2'),
        $layer_3 = $('#layer_3'),
        $layer_4 = $('#layer_4'),
        movement = 25,
        height  = movement / $(window).height(),
        width   = movement / $(window).width();
    
    $layer_container.on('mousemove', function(e){
        var pageX = e.pageX - ($(window).width()/2),
            pageY = e.pageY - ($(window).height()/2);
            $layer_1.css({
                'top':( 45 / $(window).width()) * pageY * 1.5,
                'left':( 45 / $(window).width()) * pageX * 1.5 + 100
            });
            $layer_2.css({
                'bottom':( 45 / $(window).height()) * pageY * -2 -70,
                'right':( 75 / $(window).width()) * pageX * -2 + 400
            });
            $layer_3.css({
                'top':( 35 / $(window).height())  * pageY * 1.5 -50,
                'right':( 35 / $(window).width()) * pageX * -1.5 + 100
            });
            $layer_4.css({
                'bottom':( 75 / $(window).width()) * pageY * -1.5,
                'left':( 75 / $(window).width()) * pageX * 1.5 + 200
            });
    });
    $(window).on('scroll', function(e){
        console.log($(document).scrollTop());
        if($(document).scrollTop() > 10){
            $_menu.addClass('nav-solid');
        }else{
            $_menu.removeClass('nav-solid');
        };
    });
    $_nav.on('click', function(){
        $(this).hasClass('open')?$(this).removeClass('open'):$(this).addClass('open');
    });
    $(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 750);
        return false;
      }
    }
  });
});
});
